/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package ej6;

/**
 *
 * @author nachorod
 */
public class Coche extends Vehiculo {
    private byte numPuertas;

    public byte getNumPuertas() {
        return numPuertas;
    }

    public void setNumPuertas(byte numPuertas) {
        this.numPuertas = numPuertas;
    }
    
    public Coche(String matricula) {
        super(matricula);
    }
}
