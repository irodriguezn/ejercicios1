/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package ej6;

/**
 *
 * @author nachorod
 */
public class Vehiculo {
    
    private String matricula;
    protected int velocidad;

    public Vehiculo(String matricula) {
        this.matricula = matricula;
        this.velocidad=0;
    }
    
    public void acelerar(int v) {
        velocidad+=v;
    }
    
    public String toString() {
        String cadena=  "VEHÍCULO\n";
        cadena+=        "-----\n";
        cadena+=        "Matrícula: " + matricula + "\n";
        cadena+=        "Velocidad: " + velocidad + "\n";
        return cadena;
     }    
}
