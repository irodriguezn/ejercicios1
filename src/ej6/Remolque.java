/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ej6;

/**
 *
 * @author nacho
 */
public class Remolque {
    private int peso;
    
    public Remolque(int peso) {
        this.peso=peso;
    }
    
    public String toString() {
        return "Remolque: " + peso + " Kg\n";
    }
}
