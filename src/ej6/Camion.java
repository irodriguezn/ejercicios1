/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ej6;

/**
 *
 * @author nacho
 */
public class Camion extends Vehiculo {
    Remolque remolque;
    
    public Camion(String matricula) {
        super(matricula);
    }
    
    public void ponRemolque(Remolque remolque) {
        this.remolque=remolque;
    }
    
    public void quitaRemolque() {
        remolque=null;
    }
    
    /**
     *
     * @param v
     * @throws DemasiadoRapidoExcepcion
     */
    @Override
    public void acelerar(int v) {
        try {
            if (compruebaVelocidad(velocidad+v)) {
                super.acelerar(v);
            }
        } catch (DemasiadoRapidoExcepcion ex){
            System.out.println(ex.getMessage());
        }
    }
    
    @Override
    public String toString() {
        String cadena= super.toString();
        if (remolque!=null) {
            cadena+=remolque.toString();
        }
        return cadena;
    }
    
    private boolean compruebaVelocidad(int v) throws DemasiadoRapidoExcepcion {
        if (v>100 && remolque!=null) {
            throw new DemasiadoRapidoExcepcion("El camión no puede ir tan rápido con remolque!!");
        } 
        return true;
    }
}
