/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package ej6;

/**
 *
 * @author nachorod
 */
public class AppVehiculo {
    public static void main(String[] args) {
        Vehiculo vehiculos[]=new Vehiculo[4];
        Coche co1=new Coche("M1");
        Coche co2=new Coche("M2");
        Camion ca1=new Camion("M3");
        Camion ca2=new Camion("M4");
        vehiculos[0]=co1;
        vehiculos[1]=co2;
        vehiculos[2]=ca1;
        vehiculos[3]=ca2;
        Remolque r1=new Remolque(5000);
        for (Vehiculo v:vehiculos) {
            if (v instanceof Camion) {
                ((Camion) v).ponRemolque(r1);
            }
            v.acelerar(100);
            System.out.println(v.toString());
        }
        ca2.acelerar(10);
        System.out.println(ca2.toString());
    }
}
