/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package ej4;

/**
 *
 * @author nachorod
 */
public class CocheCambioAutomatico extends Coche {

    public CocheCambioAutomatico(String matricula) {
        super(matricula);
    }
    
    public void acelerar(int v) {
        super.acelerar(v);
        marcha=calculaMarcha(getVelocidad());
    }
    
    public void frenar(int v) {
        super.frenar(v);
        marcha=calculaMarcha(getVelocidad());
    }
    
    private static byte calculaMarcha(int v) {
        byte marcha;
        if (v>=80) {
            marcha=5;
        } else if (v>=60) {
            marcha=4;
        } else if (v>=40) {
            marcha=3;
        } else if (v>=15) {
            marcha=2;
        } else if (v>0) {
            marcha=1;
        } else { marcha=0;}
        return marcha;
    }
}
