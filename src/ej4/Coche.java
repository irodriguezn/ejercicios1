/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package ej4;

/**
 *
 * @author nachorod
 */
public class Coche {
    private String matricula;
    private int velocidad;
    protected byte marcha;
    
    public Coche(String matricula) {
        this.matricula=matricula;
        velocidad=0;
        marcha=0;
    }

    public String getMatricula() {
        return matricula;
    }

    public int getVelocidad() {
        return velocidad;
    }

    public byte getMarcha() {
        return marcha;
    }
    
    public void acelerar(int v) {
        velocidad+=v;
    }
    
    public void frenar(int v) {
        velocidad-=v;
        if (velocidad<0) {
            velocidad=0;
        }
    }
    
    protected void cambiaMarcha(byte marcha) {
        this.marcha=marcha;
    }
    
    public String toString() {
        String cadena=  "COCHE\n";
        cadena+=        "-----\n";
        cadena+=        "Matrícula: " + matricula + "\n";
        cadena+=        "Velocidad: " + velocidad + "\n";
        cadena+=        "Marcha: " + marcha + "\n\n";
        return cadena;
     }
}
