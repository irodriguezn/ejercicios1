/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ej1;

/**
 *
 * @author nacho
 */
public class Hora {
    
    private int hora;
    private int minuto;
    
    public Hora(int hora, int minuto) {
        this.hora=hora;
        this.minuto=minuto;
    }
    
    public void setHora(int hora) {
        this.hora=hora;
    }
    
    public int getHora() {
        return hora;
    }
    
    public void setMinuto(int minuto) {
        this.minuto=minuto;
    }
    
    public int getMinuto() {
        return minuto;
    }
    
    public String toString() {
        return hora+":"+minuto;
    }
}