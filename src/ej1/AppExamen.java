/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ej1;

/**
 *
 * @author nacho
 */
public class AppExamen {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Fecha f1=new Fecha(4,4,2017);
        Hora h1=new Hora(8,0);
        Examen ex1=new Examen("Programación", 13, f1, h1);
        System.out.println(ex1.toString());
        f1.setAño(2016);
        h1.setMinuto(30);
        System.out.println(ex1.toString());
    }
    
}
