/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ej1;

/**
 *
 * @author nacho
 */
public class Examen {
    
    private String asignatura;
    private int aula;
    private Fecha fecha;
    private Hora hora;
    
    public Examen(String asignatura, int aula, Fecha fecha, Hora hora) {
        this.asignatura=asignatura;
        this.aula=aula;
        this.fecha=fecha;
        this.hora=hora;
    }

    public String getAsignatura() {
        return asignatura;
    }

    public void setAsignatura(String asignatura) {
        this.asignatura = asignatura;
    }

    public int getAula() {
        return aula;
    }

    public void setAula(int aula) {
        this.aula = aula;
    }

    public Fecha getFecha() {
        return fecha;
    }

    public void setFecha(Fecha fecha) {
        this.fecha = fecha;
    }

    public Hora getHora() {
        return hora;
    }

    public void setHora(Hora hora) {
        this.hora = hora;
    }
    
    public String toString() {
        String cadena="Examen"+"\n";
        cadena+="------"+"\n";
        cadena+="Asignatura: " + asignatura+"\n";
        cadena+="Aula: " + aula+"\n";
        cadena+="Fecha: " + fecha.toString()+"\n";
        cadena+="Hora: " + hora.toString()+"\n";
        return cadena;
    }
    

}
