/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package ej3;

/**
 *
 * @author nachorod
 */
public class Multimedia {
    protected String titulo;
    protected String autor;
    protected String formato;   // wav, mp3, midi, avi, mov
                                // mpg, cdAudio y dvd
    protected int duracion; // duración en minutos
    
    public Multimedia(String titulo, String autor, String formato, int duracion) {
        this.titulo=titulo;
        this.autor=autor;
        this.formato=formato;
        this.duracion=duracion;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public String getAutor() {
        return autor;
    }

    public void setAutor(String autor) {
        this.autor = autor;
    }

    public String getFormato() {
        return formato;
    }

    public void setFormato(String formato) {
        this.formato = formato;
    }

    public int getDuracion() {
        return duracion;
    }

    public void setDuracion(int duracion) {
        this.duracion = duracion;
    }
    
    public boolean equals(Multimedia m1) {
        return (m1.titulo.equals(titulo) && m1.autor.equals(autor));
    }
    
    @Override
    public String toString() {
        String respuesta="";
        respuesta+="Multimedia\n";
        respuesta+="----------\n";
        respuesta+="Titulo: " + titulo + "\n";
        respuesta+="Autor: " + autor + "\n";
        respuesta+="Formato: " + formato + "\n";
        respuesta+="Duración: " + duracion + "\n";
        return respuesta;
    }
}
