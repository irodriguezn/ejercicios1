/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ej3;

/**
 *
 * @author nacho
 */
public class AppMultimedia {
    public static void main(String[] args) {
        Pelicula p1=new Pelicula("pel1","aut1","dvd", 125, "actor1", "actriz1");
        Pelicula p2=new Pelicula("pel2","aut2","dvd", 125, "actor2", "actriz2");
        Pelicula p3=new Pelicula("pel3","aut3","dvd", 125, "actor3", "actriz3");
        Pelicula p4=new Pelicula("pel4","aut4","dvd", 125, "actor4", "actriz4");
        ListaMultimedia l1=new ListaMultimedia(10);
        if (l1.add(p1)) {
            System.out.println("Película " + p1.getTitulo() + " añadida con éxito");
        } else {
            System.out.println("Película " + p1.getTitulo() + "  no añadida a la lista");
        }

        if (l1.add(p2)) {
            System.out.println("Película " + p2.getTitulo() + " añadida con éxito");
        } else {
            System.out.println("Película " + p2.getTitulo() + "  no añadida a la lista");
        }

        if (l1.add(p3)) {
            System.out.println("Película " + p3.getTitulo() + " añadida con éxito");
        } else {
            System.out.println("Película " + p3.getTitulo() + "  no añadida a la lista");
        }

        if (l1.add(p4)) {
            System.out.println("Película " + p4.getTitulo() + " añadida con éxito");
        } else {
            System.out.println("Película " + p4.getTitulo() + "  no añadida a la lista");
        }
        System.out.println(l1.toString());
        Pelicula p5=new Pelicula("pel2", "aut2", "blue-ray", 122, "actor", "");
        int pos=l1.indexOf(p5);
        if (pos!=-1) {
            System.out.println("Pelicula encontrada en la posición " + pos);
        } else {
            System.out.println("Película no encontrada en la lista");
        }
        Multimedia m1=l1.get(pos);
        System.out.println("Posición "+ pos);
        System.out.println(m1.toString());
        ListaMultimedia discos=new ListaMultimedia(10);
        Disco d1=new Disco("Una noche en la Ópera", "Queen", "dvd", 75);
        Disco d2=new Disco("Mellon Codie and the infinite sadness", "Smashing Pumpkins", "dvd", 75);
        Disco d3=new Disco("The Wall", "Pink Floyd", "dvd", 75);
        discos.add(d1);
        discos.add(d2);
        d3.setGenero("pop/rock");
        discos.add(d3);
        System.out.println(discos.toString());
        Disco d4=new Disco("The Wall", "Pink Floyd", "", 0);
        pos=discos.indexOf(d4);
        if (pos!=-1) {
            d4=(Disco)discos.get(pos);
            System.out.println("Disco encontrado en posición " + pos);
            System.out.println(d4.toString());
        }
    }
}
