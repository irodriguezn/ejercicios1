/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package ej3;

/**
 *
 * @author nachorod
 */
public class ListaMultimedia {
    private Multimedia lista[];
    private int numElementos=0;
    
    public ListaMultimedia(int numElementos) {
        lista=new Multimedia[numElementos];
    }
    
    public int size() {
        return numElementos;
    }
    
    public boolean add(Multimedia m) {
        if (numElementos==lista.length) {
            return false;
        } else {
            // Si no hay ningún elemento inserto en la última
            // posición directamente
            if (numElementos==0) {
                lista[lista.length-1]=m;
            } else {
                int posI=lista.length-numElementos-1;
                for (int i=posI; i<lista.length-1; i++){
                    lista[i]=lista[i+1];
                }
                lista[lista.length-1]=m;
            }
            numElementos++;
            return true;
        }
    }
    
    public Multimedia get (int posicion) {
        return lista[posicion];
    }
    
    public int indexOf(Multimedia m1) {
        // [_ , _, _, _, 1, 2, 3]
        int posicion=-1;
        int posInicio=lista.length-numElementos;
        for (int i=posInicio; i<lista.length; i++) {
            if (lista[i].equals(m1)) {
                posicion=i;
                break;
            }
        }
        return posicion;
    }
    
    public String toString() {
        String resultado="";
        if (numElementos!=0) {
            for (int i=0; i<lista.length;i++) {
                if ( lista[i]!=null) {
                    resultado+= "Posición: " + i + "\n" + lista[i].toString();
                }
            }
        } else {
            resultado="No hay elementos en la lista";
        }
        return resultado;
    }
}
