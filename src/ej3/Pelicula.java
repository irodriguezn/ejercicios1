/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package ej3;

/**
 *
 * @author nachorod
 */
public class Pelicula extends Multimedia {
    private String actorPrincipal;
    private String actrizPrincipal;
    
    public Pelicula(String titulo, String autor, String formato, int duracion, String actor, String actriz) {
        super(titulo, autor, formato, duracion);
        actorPrincipal=actor;
        actrizPrincipal=actriz;
        if (actorPrincipal.equals("") && actrizPrincipal.equals("")) {
            actorPrincipal="Juanito Nadie";
            actrizPrincipal="Juanita Nadie";
        }
    }

    public String getActorPrincipal() {
        return actorPrincipal;
    }

    public void setActorPrincipal(String actorPrincipal) {
        this.actorPrincipal = actorPrincipal;
    }

    public String getActrizPrincipal() {
        return actrizPrincipal;
    }

    public void setActrizPrincipal(String actrizPrincipal) {
        this.actrizPrincipal = actrizPrincipal;
    }
    
    @Override
    public String toString() {
        String respuesta="";
        respuesta+="Multimedia\n";
        respuesta+="----------\n";
        respuesta+="Titulo: " + titulo + "\n";
        respuesta+="Autor: " + autor + "\n";
        respuesta+="Formato: " + formato + "\n";
        respuesta+="Duración: " + duracion + "\n";
        if (!actorPrincipal.equals("")) {
            respuesta+="Actor Principal: " + actorPrincipal + "\n";
        }
        if (!actrizPrincipal.equals("")) {
            respuesta+="Actriz Principal: " + actrizPrincipal + "\n";
        }
        respuesta+="\n";        
        return respuesta;
    }    
    
}
