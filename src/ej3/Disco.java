/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package ej3;

/**
 *
 * @author nachorod
 */
public class Disco extends Multimedia {
    private String genero; // pop, punk, rock, etc.
    
    public String getGenero() {
        return genero;
    }
    
    public void setGenero(String genero) {
        this.genero=genero;
    }
    
    public String toString() {
        String cadena;
        cadena=super.toString();
        cadena+="Género: " + genero + "\n\n";
        return cadena;
    }
    
    public Disco(String titulo, String autor, String formato, int duracion) {
        super(titulo, autor, formato, duracion);
    }
}
