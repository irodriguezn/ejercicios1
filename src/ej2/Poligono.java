/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package ej2;

/**
 *
 * @author nachorod
 */
public class Poligono {
    private Punto puntos[];
    private String color;
    
    public Poligono (Punto [] puntos) {
        this.puntos=puntos;
    }
    
    public String getColor() {
        return color;
    }
    
    public void setColor(String color) {
        this.color=color;
    }
    
    public void trasladar (int x, int y) {
        for (Punto p:puntos) {
            p.setX(p.getX()+x);
            p.setY(p.getY()+y);
        }
    }
    
    // Multiplicamos por la escala ambas coordenadas y
    // desplazamos de nuevo al origen.
    public void escalar (double x, double y) {
        double origenX=puntos[0].getX();
        double origenY=puntos[0].getY();
        double desplazamientoX=origenX*x-origenX;
        double desplazamientoY=origenY*y-origenY;
        
        for (Punto p:puntos) {
            p.setX(p.getX()*x-desplazamientoX);
            p.setY(p.getY()*y-desplazamientoY);
        }        
    }
    
    // Devuelve el número de vértices del polígono que coincidirá
    // con el número de puntos que lo forman. Es decir la longitud
    // del vector de puntos
    public int numVertices() {
        return puntos.length;
    }
    
    public String toString() {
        String cadena="";
        for (Punto p:puntos) {
            cadena+=p.toString()+"\n";
        }
        return cadena;
    }
    
    // El perímetro del polígono sería la suma de las distancias
    // entre los puntos que forman este
    public double perimetro() {
        double suma=0;
        for (int i=0; i<puntos.length-1; i++) {
            suma+=puntos[i].distancia(puntos[i+1]);
        }
        suma+=puntos[0].distancia(puntos[puntos.length-1]);
        return suma;
    }
}
