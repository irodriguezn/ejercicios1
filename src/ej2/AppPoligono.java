/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ej2;

/**
 *
 * @author nacho
 */
public class AppPoligono {
    public static void main(String[] args) {
        Punto p1=new Punto(0,0);
        Punto p2=new Punto(2,0);
        Punto p3=new Punto(2,2);
        Punto p4=new Punto(0,2);
        Punto cuadrado[]=new Punto[4];
        cuadrado[0]=p1;
        cuadrado[1]=p2;
        cuadrado[2]=p3;
        cuadrado[3]=p4;
        Poligono c1=new Poligono(cuadrado);
        System.out.println("Puntos cuadrado");
        System.out.println("---------------");
        System.out.println(c1.toString());
        System.out.println("Perímetro: "+ c1.perimetro());
        System.out.println("");
        c1.trasladar(4, -3);
        System.out.println("Puntos cuadrado");
        System.out.println("---------------");
        System.out.println(c1.toString());
        System.out.println("Perímetro: "+ c1.perimetro());
        System.out.println("");
        c1.trasladar(4, -3);    }
}
