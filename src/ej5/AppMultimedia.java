/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package ej5;

import ej3.*;

/**
 *
 * @author nachorod
 */
public class AppMultimedia {
    public static void main(String[] args) {
        ListaMultimedia l1=new ListaMultimedia(10);
        Disco d1=new Disco("Extremoduro", "Extremoduro", "cd", 75);
        d1.setGenero("Rock transgresivo");
        Disco d2=new Disco("Let it be", "The beatles", "cd", 75);
        d2.setGenero("Rock");
        Disco d3=new Disco("Dolitle", "Pixies", "cd", 75);
        d3.setGenero("Rock");
        Pelicula p1=new Pelicula("Qué bello es vivir!", "Frank Capra", "blueray", 122, "", "");
        Pelicula p2=new Pelicula("El Padrino", "Francis For Coppola", "blueray", 122, "Marlon Brando", "");
        Pelicula p3=new Pelicula("El Padrino II", "Francis For Coppola", "blueray", 122, "Al Pacino", "Dianne Keaton");
        l1.add(d1);
        l1.add(d2);
        l1.add(d3);
        l1.add(p1);
        l1.add(p2);
        l1.add(p3);
        int duracion=0;
        int discosRock=0;
        int noTieneActriz=0;
        Multimedia m;
        for (int i=0; i<10; i++) {
            m=l1.get(i);
            if (m!=null) {
                duracion+=m.getDuracion();
                System.out.println(m.toString());
                if ( m instanceof Disco) {
                    if (((Disco) m).getGenero().equals("Rock")) {
                        discosRock++;
                    }
                } else {
                    if (((Pelicula) m).getActrizPrincipal().equals("")) {
                        noTieneActriz++;
                    }
                    if (((Pelicula) m).getActrizPrincipal().equals("Juanita Nadie")) {
                        noTieneActriz++;
                    }                    
                }
            }
        }

        System.out.println("Duración total: " + duracion);
        System.out.println("Discos de Rock totales: " + discosRock);
        System.out.println("Películas sin actriz principal: " + noTieneActriz);
    }
}
